import PixelGameEngine from './src/PixelGameEngine';
import {Colour} from "./src/Colour";
import Grid from "./src/Grid";

export default {
	PixelGameEngine,
	Colour,
	Color: Colour,
	Grid,
}
